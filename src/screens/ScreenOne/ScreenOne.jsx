import React from "react";
import { Container, Table } from "react-bootstrap";

function ScreenOne(props) {
  //console.log(props);
  // let userData = [
  //   { id: 1, firstName: "Mark", LastName: "Otto", userName: "@mdo" },
  //   { id: 2, firstName: "Jacob", LastName: "Thornton", userName: "@fat" },
  //   { id: 3, firstName: "test", LastName: "testLastName", userName: "@test" },
  // ];

  return (
    <Container>
      <div>
        <h3>User Listing</h3>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          {props.userData.map((element) => {
            return (
              <tr key={element.id}>
                <td>{element.firstName}</td>
                <td>{element.LastName}</td>
                <td>{element.userName}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Container>
  );
}

export default ScreenOne;
