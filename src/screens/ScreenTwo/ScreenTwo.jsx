import React from "react";
import { Container, Form, Row, Col, Button } from "react-bootstrap";
function ScreenTwo(props) {

  console.log(props);

  let userInfo = {};
  let addUser = (event) => {
    let userForm = event.target.form;
    console.log(event.target.form);
    let hobbiesArray = [];
    if (userForm.chkReading.checked) {
      hobbiesArray.push("Reading");
    }
    if (userForm.chkTravaling.checked) {
      hobbiesArray.push("Travaling");
    }

    userInfo = {
      firstName: userForm.formFnameTextBox.value,
      LastName: userForm.formLnameTextBox.value,
      userName: userForm.formUnameTextBox.value,
      gender: userForm.rbMale.checked ? "Male" : "Female",
      hobbies: hobbiesArray,
      city: userForm.formSelect.value,
    };
    console.log(userInfo);
    props.onSave(userInfo);
  };

  return (
    <Container>
      <Row>
        <div>
          <h3> Add User </h3>
        </div>
      </Row>
      <Row>
        <Form>
          <Form.Group as={Row} controlId="formFnameTextBox">
            <Form.Label column sm="8">
              First Name
            </Form.Label>
            <Col sm="8">
              <Form.Control type="input" placeholder="Enter Your First Name" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formLnameTextBox">
            <Form.Label column sm="8">
              Last Name
            </Form.Label>
            <Col sm="8">
              <Form.Control type="input" placeholder="Enter Your Last Name" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formUnameTextBox">
            <Form.Label column sm="8">
              User Name
            </Form.Label>
            <Col sm="8">
              <Form.Control type="input" placeholder="Enter Your User Name" />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="formRadioButton">
            <Form.Label column sm="4">
              Gender
            </Form.Label>
            <Form.Check
              inline
              label="Male"
              type="radio"
              id="rbMale"
              name="formGender"
            ></Form.Check>
            <Form.Check
              inline
              label="Female"
              type="radio"
              id="rbFemale"
              name="formGender"
            ></Form.Check>
          </Form.Group>
          <Form.Group as={Row} controlId="formCheckBox">
            <Form.Label column sm="4">
              hobbies
            </Form.Label>
            <Form.Check
              inline
              label="Reading"
              type="checkbox"
              id="chkReading"
            ></Form.Check>
            <Form.Check
              inline
              label="Travaling"
              type="checkbox"
              id="chkTravaling"
            ></Form.Check>
          </Form.Group>
          <Form.Group controlId="formSelect">
            <Form.Label>Example select</Form.Label>
            <Form.Control as="select">
              <option>Choose Your City</option>
              <option>Delhi</option>
              <option>Ahmedabad</option>
              <option>Mumbai</option>
              <option>Surat</option>
            </Form.Control>
          </Form.Group>
          <Button
            variant="primary"
            onClick={(e) => {
              addUser(e);
            }}
          >
            Add User
          </Button>
        </Form>
      </Row>
    </Container>
  );
}
export default ScreenTwo;
