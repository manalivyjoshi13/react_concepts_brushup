import React from "react";
import { Container } from "react-bootstrap";

function ScreenFive() {
  return (
    <Container>
      <div>Welcome to screen five</div>
    </Container>
  );
}

export default ScreenFive;
