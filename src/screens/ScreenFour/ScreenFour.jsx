import React from "react";
import { Container } from "react-bootstrap";

function ScreenFour() {
  return (
    <Container>
      <div>Welcome to screen four</div>
    </Container>
  );
}

export default ScreenFour;
