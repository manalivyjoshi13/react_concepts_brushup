import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import ScreenOne from "../src/screens/ScreenOne/ScreenOne";
import ScreenTwo from "../src/screens/ScreenTwo/ScreenTwo";
import ScreenThird from "../src/screens/ScreenThird/ScreenThird";
import ScreenFour from "../src/screens/ScreenFour/ScreenFour";
import ScreenFive from "../src/screens/ScreenFive/ScreenFive";
import Header from "../src/common/Header/Header";
import { initialState } from "../src/InitialState";

//export default function Routes() {
export default class Routes extends React.Component {

  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleSave = this.handleSave.bind(this);
  }

  handleSave(userData) {
    console.log(userData);
    let data = { ...this.state };
    data.userData.push(userData);
    this.setState(data);
  }

  
  render() {

    let routesArr = (
      <>
        <Header />{" "}
        <Route
          path="/"
          exact
          component={() => <ScreenOne {...this.state}></ScreenOne>}
        ></Route>
        <Route
          path="/ScreenOne"
          exact
          component={() => <ScreenOne {...this.state}></ScreenOne>}
        ></Route>
        <Route
          path="/ScreenTwo"
          exact
          component={() => (
            <ScreenTwo {...this.state} onSave={this.handleSave}></ScreenTwo>
          )}
        ></Route>
        <Route path="/ScreenThird" exact component={ScreenThird}></Route>
        <Route path="/ScreenFour" exact component={ScreenFour}></Route>
        <Route path="/ScreenFive" exact component={ScreenFive}></Route>
      </>
    );
    
    return (
      <>
        <Router>
          <Switch>{routesArr}</Switch>
        </Router>
      </>
    );
  }
}
