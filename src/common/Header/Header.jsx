import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

function Header() {
  return (
    <>
      <Navbar bg="primary" variant="dark">
        <Navbar.Brand href="/ScreenOne">React-Brushup-POC</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/ScreenOne">
              Screen1
            </Nav.Link>
            <Nav.Link as={Link} to="/ScreenTwo">
              Screen2
            </Nav.Link>
            <Nav.Link as={Link} to="/ScreenThird">
              Screen3
            </Nav.Link>
            <Nav.Link as={Link} to="/ScreenFour">
              Screen4
            </Nav.Link>
            <Nav.Link as={Link} to="/ScreenFive">
              Screen5
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}

export default Header;
